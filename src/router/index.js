import Vue from 'vue'
import Router from 'vue-router'
import ProcessProp from '../components/page/ProcessProperties'
import ProcessListenerCfg from '../components/page/ProcessListenerCfg'
import UserTaskProp from '../components/page/task/UserTaskProperties'
import ManualTaskProp from '../components/page/task/ManualTaskProperties'
import ScriptTaskProp from '../components/page/task/ScriptTaskProperties'
import MailTaskProp from '../components/page/task/MailTaskProperties'
import ServiceTaskProp from '../components/page/task/ServiceTaskProperties'
import RecieveTaskProp from '../components/page/task/RecieveTaskProperties'
import BusinessRuleTaskProp from '../components/page/task/businessRuleTaskProperties'
import CallActivityTaskProp from '../components/page/task/CallActivityTaskProperties'
import ConnectionProp from '../components/page/connection/ConnectionProperties'
import SubProcessProp from '../components/page/container/SubProcessProperties'
import ExportXML from '../components/page/ExportXML'
import ImportXML from '../components/page/ImportXML'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'home',
      component: null
    },
    {
      path: '/process/prop',
      name: 'processProp',
      component: ProcessProp,
      children: [{
        name: 'processListener',
        path: 'listenerCfg/:id',
        component: ProcessListenerCfg,
        meta: {
          keepAlive: true
        }
      }],
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/userTask/prop',
      name: 'userTaskProp',
      component: UserTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/manaulTask/prop',
      name: 'manualTaskProp',
      component: ManualTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/scriptTask/prop',
      name: 'scriptTaskProp',
      component: ScriptTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/mailTask/prop',
      name: 'mailTaskProp',
      component: MailTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/serviceTask/prop',
      name: 'serviceTaskProp',
      component: ServiceTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/recieveTask/prop',
      name: 'recieveTaskProp',
      component: RecieveTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/businessRuleTask/prop',
      name: 'businessRuleTaskProp',
      component: BusinessRuleTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/task/callActivityTask/prop',
      name: 'callActivityTaskProp',
      component: CallActivityTaskProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/connection/prop',
      name: 'connectionProp',
      component: ConnectionProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/subProcess/prop',
      name: 'subProcessProp',
      component: SubProcessProp,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/export',
      name: 'export',
      component: ExportXML,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/import',
      name: 'import',
      component: ImportXML,
      meta: {
        keepAlive: true
      }
    }
  ]
})
